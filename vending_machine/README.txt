20170114
Kata exercise for Pillar
Colin R. Stoetzner


1) Literal instructions:

-The literal instructions here apply to a windows command line environment.

-This readme contains command line instructions. If you see ">>TEXT" (minus the quotes) that means enter the literal text "TEXT" (minus the quotes) at the command prompt and press enter. So,

>>gradlew

...would mean to exactly type the word "gradlew" (minus quotes) at the commmand prompt and press enter.


2) Build instructions:

-This exercise uses Gradle to build, test and run a Java application and associated tests.

-At minimum a Java 7 runtime should be installed to support both the application and the Gradle command line environment. On Windows, the %JAVA_HOME% environment variable should be set to the JRE or JDK installation directory.

-Eclipse project files are present in the repository but can be ignored.

-The Gradle wrapper is included in the repository and should be used to interact with the project through the command line.

-After cloning this repository, navigate to the root directory and open a command prompt. 

>>gradlew tasks

-Note a section of tasks specific for this exercise:

Kata Demo tasks
---------------
runTests - View individual test results in console.

>>gradlew runTests

...will run all individual unit tests in the project and give feedback about individual results in the console (also building out of date portions of the application). Note that the following built-in task,

>>gradlew test

...may not show individual test results in the console and is not recommended.

-See build.gradle for build script implementation details, if desired.