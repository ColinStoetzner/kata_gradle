package vendingMachine;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class VendingMachine {

	public enum Coin {
		QUARTER(5.670, 1.75), DIME(2.268, 1.35), NICKEL(5.0, 1.95), PENNY(2.5, 1.55);
		
		double weightGrams;
		double thicknessMillimeters;

		private Coin(double weightGrams, double thicknessMillimeters) {
			this.weightGrams = weightGrams;
			this.thicknessMillimeters = thicknessMillimeters;
		}
	}

	public enum Product {
		NULL("0.00"), COLA("1.00"), CHIPS("0.50"), CANDY("0.65");
		BigDecimal price;
		private Product(String price) {
			this.price = new BigDecimal(price);
		}
	}

	private enum State {
		EMPTY, SOME_FUNDS, VENDING, INSUFFICIENT_FUNDS;
	}
	
	private List<Coin> coinReturnContents = new ArrayList<Coin>();
	private CoinDetector coinDetector = new CoinDetector();
	private List<Product> productDispenserContents = new ArrayList<Product>();
	final private BigDecimal zeroDollars = new BigDecimal("0");
	private BigDecimal dollarsAccepted = zeroDollars;
	private Product desiredProduct = Product.NULL;
	private State currentState = State.EMPTY;
	
	public String getDisplayMessage() {
		switch (currentState) {
		case EMPTY:
			return "INSERT COIN";
		case SOME_FUNDS:
			return showCurrentAmountOfMoneyInSystem();
		case VENDING:
			updateStateFromDollarAmount();
			return "THANK YOU";
		case INSUFFICIENT_FUNDS:
			updateStateFromDollarAmount();
			return showPriceOfDesiredProduct();
		default:
			return "";
		}
	}

	public void insertCoin(Coin coin) {
		BigDecimal valueForCoin = coinDetector.determineValueOfCoin(coin);
		if (isPennyDetected(valueForCoin)){
			this.coinReturnContents.add(coin);			
		} else {
			increaseDollarsAcceptedBy(valueForCoin);			
		}
		updateStateFromDollarAmount();
	}

	public List<Coin> getContentsOfCoinReturn() {
		List<Coin> coinReturnContentsForDelivery = copyForDelivery(this.coinReturnContents);
		clearOutCoinDispenser();
		return coinReturnContentsForDelivery;
	}

	public void pressButtonForCola() {
		pressButtonFor(Product.COLA);		
	}

	public void pressButtonForChips() {
		pressButtonFor(Product.CHIPS);		
	}
	
	public void pressButtonForCandy() {
		pressButtonFor(Product.CANDY);		
	}
	
	public List<Product> getDispensedProducts() {
		List<Product> productDispenserContentsToReturn = copyForDelivery(this.productDispenserContents);
		clearOutProductDispener();
		return productDispenserContentsToReturn;
	}

	public void pressButtonToReturnUnusedCoins() {
		returnChange();
		updateStateFromDollarAmount();
	}
	
	private String showCurrentAmountOfMoneyInSystem() {
		return displayInStandardCurrencyFormat(this.dollarsAccepted);
	}

	private String showPriceOfDesiredProduct() {
		return "PRICE: " + displayInStandardCurrencyFormat(desiredProduct.price);
	}
	
	private String displayInStandardCurrencyFormat(BigDecimal dollarValue) {
		return String.format("$%.2f", dollarValue);
	}
	
	private void pressButtonFor(Product product) {
		if (hasEnoughMoneyToPurchase(product)) {
			collectPurchasePrice(product);
			dispenseProduct(product);			
			returnChange();
		} else {
			this.currentState = State.INSUFFICIENT_FUNDS;
		}
		this.desiredProduct = product;
	}
	
	private boolean hasEnoughMoneyToPurchase(Product product) {
		return this.dollarsAccepted.compareTo(product.price) >= 0;
	}
	
	private void collectPurchasePrice(Product product) {
		this.dollarsAccepted = this.dollarsAccepted.subtract(product.price);
	}

	private void dispenseProduct(Product product) {
		this.productDispenserContents.add(product);
		this.currentState = State.VENDING;
	}
	
	private void returnChange() {
		List<Coin> coinsToReturn = new CoinSet(this.dollarsAccepted).getCoins();
		this.coinReturnContents.addAll(coinsToReturn);
		this.dollarsAccepted = zeroDollars;
	}

	private boolean isPennyDetected(BigDecimal valueForCoin) {
		return valueForCoin.equals(coinDetector.valueOfPenny);
	}
	
	private BigDecimal increaseDollarsAcceptedBy(BigDecimal valueForCoin) {
		return this.dollarsAccepted = this.dollarsAccepted.add(valueForCoin);
	}
	
	private void updateStateFromDollarAmount(){
		if (this.dollarsAccepted.compareTo(zeroDollars)==1){
			this.currentState = State.SOME_FUNDS;
			return; 
		}
		this.currentState = State.EMPTY;
	}
	
	private <T> List<T> copyForDelivery(List<T> originalContents) {
		List<T> newDeliveryContainer = new ArrayList<T>();
		for (T element : originalContents) {
			newDeliveryContainer.add(element);
		}
		return newDeliveryContainer;
	}

	private void clearOutCoinDispenser() {
		this.coinReturnContents = new ArrayList<Coin>();
	}
	
	private void clearOutProductDispener() {
		this.productDispenserContents = new ArrayList<Product>();
	}	
}
