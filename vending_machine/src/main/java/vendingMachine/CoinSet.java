package vendingMachine;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import vendingMachine.VendingMachine.Coin;

public class CoinSet {

	private BigDecimal desiredValue;
	
	public CoinSet(BigDecimal desiredValue) {
		this.desiredValue = desiredValue;
	}

	public List<Coin> getCoins() {
		List<Coin> coins = new ArrayList<Coin>();
		while(isRemainderOfDesiredValueGreaterThanZero()){
			if (isRemainderOfDesiredValueLessThan(CoinDetector.valueOfDime)) {
				subtractValueFromRemainder(CoinDetector.valueOfNickel);
				coins.add(Coin.NICKEL);				
			} else if (isRemainderOfDesiredValueLessThan(CoinDetector.valueOfQuarter)) {
				subtractValueFromRemainder(CoinDetector.valueOfDime);
				coins.add(Coin.DIME);				
			} else {
				subtractValueFromRemainder(CoinDetector.valueOfQuarter);
				coins.add(Coin.QUARTER);				
			}
		}
		return coins;
	}

	private BigDecimal subtractValueFromRemainder(BigDecimal value) {
		return this.desiredValue = this.desiredValue.subtract(value);
	}

	private boolean isRemainderOfDesiredValueLessThan(BigDecimal value) {
		return this.desiredValue.compareTo(value) == -1;
	}

	private boolean isRemainderOfDesiredValueGreaterThanZero() {
		return this.desiredValue.compareTo(new BigDecimal("0")) == 1;
	}
}
