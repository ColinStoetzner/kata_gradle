package vendingMachine;

import java.math.BigDecimal;

import vendingMachine.VendingMachine.Coin;

public class CoinDetector {

	static BigDecimal valueOfNickel = new BigDecimal("0.05");
	static BigDecimal valueOfQuarter = new BigDecimal("0.25");
	static BigDecimal valueOfDime = new BigDecimal("0.10");
	BigDecimal valueOfPenny = new BigDecimal("0.01");
	
	BigDecimal determineValueOfCoin(Coin coin) {
		if (!isCoinThinEnoughToPassThroughWideSlot(coin)){
			return valueOfNickel;
		}
		if (isCoinHeavyEnoughToActivateSensor(coin)) {
			return valueOfQuarter;
		}
		if (isCoinThinEnoughToPassThroughNarrowSlot(coin)){
			return valueOfDime;
		}
		return valueOfPenny;
	}
	
	private boolean isCoinThinEnoughToPassThroughWideSlot(Coin coin) {
		double thicknessOfWideSlotInMillimeters = 1.8;
		return coin.thicknessMillimeters < thicknessOfWideSlotInMillimeters;
	}
	
	private boolean isCoinThinEnoughToPassThroughNarrowSlot(Coin coin) {
		double thicknessOfNarrowSlotInMillimeters = 1.4;
		return coin.thicknessMillimeters < thicknessOfNarrowSlotInMillimeters;
	}
	
	private boolean isCoinHeavyEnoughToActivateSensor(Coin coin) {
		double weightCriteriaForSensorInGrams = 2.5;
		return coin.weightGrams > weightCriteriaForSensorInGrams;
	}
}
