package vendingMachine;


import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

import vendingMachine.VendingMachine.Coin;


public class CoinDetectorTest {

	CoinDetector coinDetector = new CoinDetector();
	
	@Test
	public void detectQuarterByPhysicalProperties() throws Exception {
		assertEquals(new BigDecimal("0.25"), coinDetector.determineValueOfCoin(Coin.QUARTER));
	}
	
	@Test
	public void detectDimeByPhysicalProperties() throws Exception {
		assertEquals(new BigDecimal("0.10"), coinDetector.determineValueOfCoin(Coin.DIME));
	}
	
	@Test
	public void detectNickelByPhysicalProperties() throws Exception {
		assertEquals(new BigDecimal("0.05"), coinDetector.determineValueOfCoin(Coin.NICKEL));
	}
	
	@Test
	public void detectPennyByPhysicalProperties() throws Exception {
		assertEquals(new BigDecimal("0.01"), coinDetector.determineValueOfCoin(Coin.PENNY));
	}
}
