package vendingMachine;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import vendingMachine.VendingMachine.Coin;
import vendingMachine.VendingMachine.Product;

public class VendingMachineTest extends CommonTestTools {

	VendingMachine vendingMachine = new VendingMachine();

	@Test
	public void machineStartsWithEmptyDispensersAndWithoutFunds() throws Exception {
		checkThatDisplayRequestsCoins();
		checkThatCoinReturnIsEmpty();
		checkThatProductDispenserIsEmpty();
	}

	@Test
	public void machineIgnoresAPennyIfInserted() throws Exception {
		vendingMachine.insertCoin(Coin.PENNY);
		checkThatDisplayRequestsCoins();
	}
	
	@Test
	public void aPennyGoesToTheCoinReturn() throws Exception {
		vendingMachine.insertCoin(Coin.PENNY);
		List<Coin> contentsOfCoinReturn = vendingMachine.getContentsOfCoinReturn();
		assertEquals(1, contentsOfCoinReturn.size());
		assertEquals(Coin.PENNY, contentsOfCoinReturn.get(0));
	}
	
	@Test
	public void insertAFewPenniesAndFindThemAllInTheCoinReturn() throws Exception {
		vendingMachine.insertCoin(Coin.PENNY);
		vendingMachine.insertCoin(Coin.PENNY);
		assertEquals(2, vendingMachine.getContentsOfCoinReturn().size());		
	}
	
	@Test
	public void acceptedCoinsDoNotGoToTheCoinReturn() throws Exception {
		vendingMachine.insertCoin(Coin.NICKEL);
		vendingMachine.insertCoin(Coin.QUARTER);
		vendingMachine.insertCoin(Coin.DIME);
		checkThatCoinReturnIsEmpty();
	}
	
	@Test
	public void insertTwoNickelsAndDisplayTenCents() throws Exception {
		vendingMachine.insertCoin(Coin.NICKEL);
		vendingMachine.insertCoin(Coin.NICKEL);
		assertEquals("$0.10", vendingMachine.getDisplayMessage());
	}
	
	@Test
	public void insertOneDimeAndOneQuarterAndDisplayThirtyFiveCents() throws Exception {
		vendingMachine.insertCoin(Coin.DIME);
		vendingMachine.insertCoin(Coin.QUARTER);
		assertEquals("$0.35", vendingMachine.getDisplayMessage());		
	}
	
	@Test
	public void customerCanGetColaForADollar() throws Exception {
		insertADollar();
		vendingMachine.pressButtonForCola();
		assertEquals(Product.COLA, vendingMachine.getDispensedProducts().get(0));		
	}

	@Test
	public void sayThankYouOnceAfterDispensingACola() throws Exception {
		insertADollar();
		vendingMachine.pressButtonForCola();
		assertEquals("THANK YOU", vendingMachine.getDisplayMessage());		
		checkThatDisplayRequestsCoins();		
	}
	
	@Test
	public void showColaPriceAndAskForCoinsIfNoneDeposited() throws Exception {
		vendingMachine.pressButtonForCola();
		checkForDisplayOfColaPrice();				
		checkThatDisplayRequestsCoins();		
	}

	@Test
	public void getChipsForFiftyCents() throws Exception {
		vendingMachine.insertCoin(Coin.QUARTER);
		vendingMachine.insertCoin(Coin.QUARTER);
		vendingMachine.pressButtonForChips();
		checkForExpectedProduct(Product.CHIPS);
	}
	
	@Test
	public void showPriceOfChipsIfOnlyDepositedTenCents() throws Exception {
		vendingMachine.insertCoin(Coin.DIME);
		vendingMachine.pressButtonForChips();
		assertEquals("PRICE: $0.50", vendingMachine.getDisplayMessage());						
	}
	
	@Test
	public void getCandyForSixtyFiveCents() throws Exception {
		vendingMachine.insertCoin(Coin.DIME);
		vendingMachine.insertCoin(Coin.NICKEL);
		vendingMachine.insertCoin(Coin.QUARTER);
		vendingMachine.insertCoin(Coin.QUARTER);
		vendingMachine.pressButtonForCandy();
		checkForExpectedProduct(Product.CANDY);						
	}

	@Test
	public void noFreeProductsJustForPressingButtons() throws Exception {
		vendingMachine.pressButtonForCandy();
		vendingMachine.pressButtonForChips();
		vendingMachine.pressButtonForCola();
		checkThatProductDispenserIsEmpty();
	}
	
	@Test
	public void getThirtyFiveCentsBackFromADollarIfBuyingCandy() throws Exception {
		insertADollar();
		vendingMachine.pressButtonForCandy();
		List<Coin> contents = vendingMachine.getContentsOfCoinReturn();
		assertEquals(2, contents.size());
		String message = "assume largest coins first";
		assertEquals(message, Coin.QUARTER, contents.get(0));
		assertEquals(message, Coin.DIME, contents.get(1));
	}
	
	@Test
	public void returnOneNickelInChange() throws Exception {
		vendingMachine.insertCoin(Coin.QUARTER);
		vendingMachine.insertCoin(Coin.QUARTER);
		vendingMachine.insertCoin(Coin.NICKEL);
		vendingMachine.pressButtonForChips();
		List<Coin> contents = vendingMachine.getContentsOfCoinReturn();
		assertEquals(1, contents.size());
		assertEquals(Coin.NICKEL, contents.get(0));
	}

	@Test
	public void returnADollarInQuartersIfPurchaseIsNotDesired() throws Exception {
		insertADollar();
		vendingMachine.pressButtonToReturnUnusedCoins();
		Iterator<Coin> coinsReturned = vendingMachine.getContentsOfCoinReturn().iterator();
		checkForNextCoin(Coin.QUARTER, coinsReturned);
		checkForNextCoin(Coin.QUARTER, coinsReturned);
		checkForNextCoin(Coin.QUARTER, coinsReturned);
		checkForNextCoin(Coin.QUARTER, coinsReturned);
		assertFalse(coinsReturned.hasNext());
	}

	@Test
	public void coinReturnDoesNotGiveFreeMoney() throws Exception {
		vendingMachine.pressButtonToReturnUnusedCoins();
		checkThatCoinReturnIsEmpty();
	}
	
	@Test
	public void returnCoinsButtonUpdatesDisplayToRequestCoins() throws Exception {
		vendingMachine.insertCoin(Coin.NICKEL);
		assertEquals("$0.05", vendingMachine.getDisplayMessage());	
		vendingMachine.pressButtonToReturnUnusedCoins();
		checkThatDisplayRequestsCoins();
	}
	
	@Test
	public void collectingCoinsOnceClearsThemFromTheReturn() throws Exception {
		vendingMachine.insertCoin(Coin.DIME);
		vendingMachine.pressButtonToReturnUnusedCoins();
		Iterator<Coin> returnedCoins = vendingMachine.getContentsOfCoinReturn().iterator();
		checkForNextCoin(Coin.DIME, returnedCoins);
		assertFalse(returnedCoins.hasNext());

		checkThatCoinReturnIsEmpty();
	}
		
	@Test
	public void collectingProductsOnceClearsThemFromTheReturn() throws Exception {
		insertADollar();
		vendingMachine.pressButtonForCola();
		checkForExpectedProduct(Product.COLA);
		
		checkThatProductDispenserIsEmpty();
	}
	
	@Test
	public void canCollectTwoItemsAfterMakingAllPurchases() throws Exception {
		insertADollar();
		vendingMachine.pressButtonForCola();
		insertADollar();
		vendingMachine.pressButtonForChips();
		Iterator<Product> products = vendingMachine.getDispensedProducts().iterator();
		assertEquals(Product.COLA, products.next());
		assertEquals(Product.CHIPS, products.next());
		assertFalse(products.hasNext());
	}
	
	@Test
	public void canCollectAllChangeAfterTwoSeparateRefunds() throws Exception {
		vendingMachine.insertCoin(Coin.QUARTER);
		vendingMachine.pressButtonToReturnUnusedCoins();
		vendingMachine.insertCoin(Coin.DIME);
		vendingMachine.pressButtonToReturnUnusedCoins();
		Iterator<Coin> products = vendingMachine.getContentsOfCoinReturn().iterator();
		assertEquals(Coin.QUARTER, products.next());
		assertEquals(Coin.DIME, products.next());
		assertFalse(products.hasNext());
	}
	
	private void checkForExpectedProduct(Product expectedProduct) {
		List<Product> dispensedProducts = vendingMachine.getDispensedProducts();
		assertEquals("expecting at product", 1, dispensedProducts.size());
		assertEquals(expectedProduct, dispensedProducts.get(0));
	}
	
	private void insertADollar() {
		vendingMachine.insertCoin(Coin.QUARTER);
		vendingMachine.insertCoin(Coin.QUARTER);
		vendingMachine.insertCoin(Coin.QUARTER);
		vendingMachine.insertCoin(Coin.QUARTER);
	}
	
	private void checkThatProductDispenserIsEmpty() {
		assertEquals(0, vendingMachine.getDispensedProducts().size());
	}

	private void checkThatCoinReturnIsEmpty() {
		assertEquals(0, vendingMachine.getContentsOfCoinReturn().size());
	}
	
	private void checkThatDisplayRequestsCoins() {
		assertEquals("INSERT COIN", vendingMachine.getDisplayMessage());
	}
	
	private void checkForDisplayOfColaPrice() {
		assertEquals("PRICE: $1.00", vendingMachine.getDisplayMessage());
	}	
}

