package vendingMachine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import vendingMachine.VendingMachine.Coin;

public class CoinSetTest extends CommonTestTools{

	@Test
	public void makeASetWithOneQuarter() throws Exception {
		CoinSet coinSet = new CoinSet(new BigDecimal("0.25"));
		List<Coin> coins = coinSet.getCoins();
		assertEquals(1, coins.size());
		assertEquals(Coin.QUARTER, coins.get(0));
	}
	
	@Test
	public void makeADollarWithFourQuarters() throws Exception {
		CoinSet coinSet = new CoinSet(new BigDecimal("1.00"));
		Iterator<Coin> coins = coinSet.getCoins().iterator();
		checkForNextCoin(Coin.QUARTER, coins);
		checkForNextCoin(Coin.QUARTER, coins);
		checkForNextCoin(Coin.QUARTER, coins);
		checkForNextCoin(Coin.QUARTER, coins);
		assertFalse(coins.hasNext());
	}

	@Test
	public void useQuarterAndNickelToMakeThirtyCents() throws Exception {
		CoinSet coinSet = new CoinSet(new BigDecimal("0.30"));
		Iterator<Coin> coins = coinSet.getCoins().iterator();
		checkForNextCoin(Coin.QUARTER, coins);
		checkForNextCoin(Coin.NICKEL, coins);
		assertFalse(coins.hasNext());
	}
	
	@Test
	public void useQuarterDimeAndNickelToMakeFortyCents() throws Exception {
		CoinSet coinSet = new CoinSet(new BigDecimal("0.40"));
		Iterator<Coin> coins = coinSet.getCoins().iterator();
		checkForNextCoin(Coin.QUARTER, coins);
		checkForNextCoin(Coin.DIME, coins);
		checkForNextCoin(Coin.NICKEL, coins);
		assertFalse(coins.hasNext());
	}

}
