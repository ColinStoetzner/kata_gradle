package vendingMachine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import vendingMachine.VendingMachine.Coin;

public class CommonTestTools {

	protected void checkForNextCoin(Coin expectedCoin, Iterator<Coin> coins) {
		assertTrue("expected " + expectedCoin, coins.hasNext());
		assertEquals(expectedCoin, coins.next());
	}	
}
