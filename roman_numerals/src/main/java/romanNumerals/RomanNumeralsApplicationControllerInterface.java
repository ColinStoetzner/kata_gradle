package romanNumerals;

public interface RomanNumeralsApplicationControllerInterface {
	
	public void convertFromCommandLineArgs(String[] args);

	public void convertFromUI(String text);
}
