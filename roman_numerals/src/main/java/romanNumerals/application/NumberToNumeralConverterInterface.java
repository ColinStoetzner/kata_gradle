package romanNumerals.application;

public interface NumberToNumeralConverterInterface {

	public String convertNumberToNumeral(int number);

}
