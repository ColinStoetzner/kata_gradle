package romanNumerals.application;

public interface LaunchArgumentParserInterface {

	public abstract int getNumberToConvert();

	public abstract boolean foundValidNumber();

	public abstract void parse(String[] args);

}
