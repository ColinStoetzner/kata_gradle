package romanNumerals.application;

import romanNumerals.RomanNumeralsApplicationControllerInterface;
import romanNumerals.RomanNumeralsUIInterface;
import romanNumerals.commandLine.ConsoleOutput;
import romanNumerals.commandLine.LaunchArgumentParser;
import romanNumerals.conversion.NumberToNumeralConverter;
import romanNumerals.ui.RomanNumeralsUI;

public class RomanNumeralsApplicationController implements RomanNumeralsApplicationControllerInterface{

	public static interface TextInputDispatchInterface {
		public void processInput(String textInput);
	}

	public static class TextInputDispatchedToConverter implements TextInputDispatchInterface{

		private RomanNumeralsApplicationControllerInterface controller;

		TextInputDispatchedToConverter(
				RomanNumeralsApplicationControllerInterface controller) {
					this.controller = controller;
		}
		
		public RomanNumeralsApplicationControllerInterface getController() {
			return controller;
		}

		@Override
		public void processInput(String textInput) {
			this.controller.convertFromUI(textInput);
		}
	}
	
	private NumberToNumeralConverterInterface converter;
	private LaunchArgumentParserInterface parser;
	private ConsoleOutputInterface consoleOut;
	private RomanNumeralsUIInterface ui;

	RomanNumeralsApplicationController(
			NumberToNumeralConverterInterface converter,
			LaunchArgumentParserInterface parser,
			ConsoleOutputInterface consoleOut, 
			RomanNumeralsUIInterface ui) {
				this.converter = converter;
				this.parser = parser;
				this.consoleOut = consoleOut;
				this.ui = ui;
	}

	public NumberToNumeralConverterInterface getConverter() {
		return this.converter;
	}

	public LaunchArgumentParserInterface getParser() {
		return this.parser;
	}

	public ConsoleOutputInterface getConsoleOut() {
		return this.consoleOut;
	}
	
	public RomanNumeralsUIInterface getUI() {
		return this.ui;
	}

	@Override
	public void convertFromCommandLineArgs(String[] args) {
		this.parser.parse(args);
		if (this.parser.foundValidNumber()){
			int numberToConvert = this.parser.getNumberToConvert();
			String numeral = this.converter.convertNumberToNumeral(numberToConvert);
			this.consoleOut.sendToConsole(numeral);
		} else {
			String notification = 
					"No arguments contain valid numeric input,"
					+ " launching UI by default.";
			this.consoleOut.sendToConsole(notification);
			launchAndShowUI();
			connectSelfAsListenerForTextInput();
		}
	}

	@Override
	public void convertFromUI(String text) {
		this.parser.parse(new String[]{text});
		if(this.parser.foundValidNumber()){
			int numberToConvert = this.parser.getNumberToConvert();
			String numeral = converter.convertNumberToNumeral(numberToConvert);
			this.ui.setNumeralDisplay(numeral);
		} else {
			this.ui.setNumeralDisplay("");
		};
	}

	private void launchAndShowUI() {
		this.ui.initializeComponents();
		this.ui.show();
	}

	private void connectSelfAsListenerForTextInput() {
		this.ui.listenForTranslationRequests(new TextInputDispatchedToConverter(this));
	}
	
	public static RomanNumeralsApplicationControllerInterface create() {
		NumberToNumeralConverterInterface converter = NumberToNumeralConverter.create();
		RomanNumeralsUIInterface ui = RomanNumeralsUI.build();
		return new RomanNumeralsApplicationController(
				converter,
				new LaunchArgumentParser(),
				new ConsoleOutput(),
				ui);
		
	}
}
