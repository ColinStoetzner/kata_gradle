package romanNumerals.application;

public interface ConsoleOutputInterface {
	public void sendToConsole(String output);
}
