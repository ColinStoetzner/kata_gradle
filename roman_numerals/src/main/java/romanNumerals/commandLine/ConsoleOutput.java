package romanNumerals.commandLine;

import romanNumerals.application.ConsoleOutputInterface;

public class ConsoleOutput implements ConsoleOutputInterface{

	@Override
	public void sendToConsole(String output) {
		System.out.println(output);
	}

}
