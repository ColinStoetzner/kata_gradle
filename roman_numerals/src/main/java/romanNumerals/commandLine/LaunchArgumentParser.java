package romanNumerals.commandLine;

import romanNumerals.application.LaunchArgumentParserInterface;


public class LaunchArgumentParser implements LaunchArgumentParserInterface{

	private String[] currentArgs;
	private int validNumberRequest = 0;
	private boolean hasValidNumberRequest = false;

	@Override
	public void parse(String[] args) {
		reset();
		if (args==null){
			return;
		}
		this.currentArgs = args;
		storeFirstValidNumericalArgument();
	}

	String[] getCurrentArgs() {
		return currentArgs;
	}

	@Override
	public boolean foundValidNumber() { 
		return this.hasValidNumberRequest;
	}

	@Override
	public int getNumberToConvert() {
		return this.validNumberRequest;
	}
	
	private void reset() {
		this.hasValidNumberRequest = false;
		this.validNumberRequest = 0;
	}

	private void storeFirstValidNumericalArgument() {
		for (int i = 0; i < this.currentArgs.length; i++) {
			try {
				int numberRequest = Integer.parseInt(this.currentArgs[i]);
				this.validNumberRequest = numberRequest;
				this.hasValidNumberRequest = true;
				break;
			} catch (Exception e) {
				continue;
			}
		}
	}
}
