package romanNumerals.conversion;

import java.util.Iterator;

public class NumberToNumeralPreprocessor implements NumberToNumeralPreprocessorInterface{

	private String outputBuffer;
	private NumberAndNumeralDictionary dictionary;

	NumberToNumeralPreprocessor() {
		dictionary = new NumberAndNumeralDictionary();
	}
	
	public String convertToNumeralWithoutSubstitution(int number) {		
		clearOutputBuffer();
		Iterator<Integer> divisors = dictionary.getDescendingNumberEntriesIterator();
		int remainder = number;
		while (divisors.hasNext()) {
			remainder = addNumeralForEachQuotient(remainder, divisors.next());
		}
		return outputBuffer;
	}

	private void clearOutputBuffer() {
		outputBuffer = "";
	}

	private int addNumeralForEachQuotient(int dividend, int divisor) {
		int quotient = dividend / divisor; 
		for (int i = 0; i < quotient; i++) {
			outputBuffer += dictionary.getNumeral(divisor);
		}
		int remainder = dividend % divisor;
		return remainder;
	}
}
