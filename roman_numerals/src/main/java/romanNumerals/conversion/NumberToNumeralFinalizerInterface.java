package romanNumerals.conversion;

public interface NumberToNumeralFinalizerInterface {

	public String substituteNumeralsIfNeeded(String numeralString);

}
