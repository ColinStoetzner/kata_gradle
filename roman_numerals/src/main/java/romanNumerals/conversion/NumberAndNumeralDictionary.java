package romanNumerals.conversion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class NumberAndNumeralDictionary {

	private LinkedHashMap<Integer, String> descendingPrimitiveNumeralsByValue;
	private Map<String, Integer> descendingPrimitiveNumeralsEntryIndex;
	private List<String> descendingPrimitiveNumerals;

	
	
	NumberAndNumeralDictionary() {
		descendingPrimitiveNumeralsByValue = new LinkedHashMap<Integer, String>();
		descendingPrimitiveNumeralsEntryIndex = new HashMap<String, Integer>();
		descendingPrimitiveNumerals = new ArrayList<String>();
		
		descendingPrimitiveNumeralsByValue.put(1000, "M");
		descendingPrimitiveNumeralsByValue.put(500, "D");
		descendingPrimitiveNumeralsByValue.put(100, "C");
		descendingPrimitiveNumeralsByValue.put(50, "L");
		descendingPrimitiveNumeralsByValue.put(10, "X");
		descendingPrimitiveNumeralsByValue.put(5, "V");
		descendingPrimitiveNumeralsByValue.put(1, "I");
			
		int index = 0;
		for (Entry<Integer, String> entry : descendingPrimitiveNumeralsByValue.entrySet()) {
			String numeral = entry.getValue();
			descendingPrimitiveNumeralsEntryIndex.put(numeral, index++);
			descendingPrimitiveNumerals.add(numeral);
		}
	}
	
	public String getNumeral(int number) {
		String numeralQueryResult = descendingPrimitiveNumeralsByValue.get(number);
		if (numeralQueryResult==null){
			return "";
		}
		return numeralQueryResult;
	}

	public Iterator<Integer> getDescendingNumberEntriesIterator() {
		return descendingPrimitiveNumeralsByValue.keySet().iterator();
	}

	public String nextLargestIndividualNumeral(String numeral) {
		Integer queryForIndex = descendingPrimitiveNumeralsEntryIndex.get(numeral);
		boolean invalidQuery = queryForIndex==null;
		if (invalidQuery || hasNoPrevious(queryForIndex)){
			return numeral;
		}
		return descendingPrimitiveNumerals.get(queryForIndex-1);
	}

	private boolean hasNoPrevious(Integer query) {
		return query==0;
	}

	public Iterator<String> getDescendingRomanNumeralsIterator() {
		return descendingPrimitiveNumeralsByValue.values().iterator();
	}

	public boolean canIncrementNumeral(String queryNumeral) {
		return !queryNumeral.equals(nextLargestIndividualNumeral(queryNumeral));
	}
}
