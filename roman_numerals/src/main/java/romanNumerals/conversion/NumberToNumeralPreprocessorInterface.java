package romanNumerals.conversion;

public interface NumberToNumeralPreprocessorInterface {

	public String convertToNumeralWithoutSubstitution(int number);

}
