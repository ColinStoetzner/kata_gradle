package romanNumerals.conversion;

import romanNumerals.application.NumberToNumeralConverterInterface;

public class NumberToNumeralConverter implements NumberToNumeralConverterInterface{

	private NumberToNumeralPreprocessorInterface preprocessor;
	private NumberToNumeralFinalizerInterface finalizer;

	NumberToNumeralConverter(NumberToNumeralPreprocessorInterface preprocessor,
			NumberToNumeralFinalizerInterface finalizer) {
		this.preprocessor = preprocessor;
		this.finalizer = finalizer;
	}

	public NumberToNumeralPreprocessorInterface getPreprocessor() {
		return this.preprocessor;
	}

	public NumberToNumeralFinalizerInterface getFinalizer() {
		return this.finalizer;
	}

	@Override
	public String convertNumberToNumeral(int number) {
		String preprocessedNumerals = this.preprocessor.convertToNumeralWithoutSubstitution(number);
		String finalizedNumerals = this.finalizer.substituteNumeralsIfNeeded(preprocessedNumerals);
		return finalizedNumerals;
	}

	public static NumberToNumeralConverterInterface create() {
		NumberToNumeralFinalizer finalizerForFactory = new NumberToNumeralFinalizer();
		return new NumberToNumeralConverter(new NumberToNumeralPreprocessor(), finalizerForFactory);
	}
}
