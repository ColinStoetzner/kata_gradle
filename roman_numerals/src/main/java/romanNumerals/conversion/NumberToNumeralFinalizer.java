package romanNumerals.conversion;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NumberToNumeralFinalizer implements NumberToNumeralFinalizerInterface{

	public static class SubstituteAndIncrementAgent {
		private String letterToScan;
		private String quadRepeat = "";
		private final int searchFailureCode = -1;
		private boolean isLargerNumeralInDictionary;
		private String nextLargestIndividualNumeral = "";
		private boolean isTwiceLargerNumeralInDictionary;
		private String twiceLargestNumeral = "";
		
		SubstituteAndIncrementAgent(String substitutionTarget) {
			this.letterToScan = substitutionTarget;
			for (int i = 0; i < 4; i++) {
				this.quadRepeat += this.letterToScan;
			}
			this.isLargerNumeralInDictionary = dictionary.canIncrementNumeral(this.letterToScan);
			if (this.isLargerNumeralInDictionary){
				this.nextLargestIndividualNumeral = dictionary.nextLargestIndividualNumeral(this.letterToScan);				
			}
			this.isTwiceLargerNumeralInDictionary = dictionary.canIncrementNumeral(this.nextLargestIndividualNumeral);
			if (this.isTwiceLargerNumeralInDictionary){
				this.twiceLargestNumeral = dictionary.nextLargestIndividualNumeral(this.nextLargestIndividualNumeral);
			}
		}

		public String getLetterToSubstitute() {
			return this.letterToScan;
		}

		public String substituteQuadRepeats(String candidate) {
			if (cannotIncrementNumeral()){
				return candidate;
			}
			
			int indexBeginningOfQuad = candidate.indexOf(this.quadRepeat);
			if (doesNotContainQuadRepeatForNumeral(indexBeginningOfQuad)){
				return candidate;
			}
			
			return substituteAndIncrementNumerals(
					candidate, indexBeginningOfQuad);
		}

		private String substituteAndIncrementNumerals(String candidate,
				int indexBeginningOfQuad) {
			String leadingPortionOfCandidate = "";
			String substitutedLetter = this.letterToScan;
			String letterToInsert = "";
			String candidateTrailingRegion = getCandidateTrailingRegion(candidate, indexBeginningOfQuad);						
			
			int indexOfNumeralToIncrement = indexBeginningOfQuad-1;
			boolean canAndShouldIncrementLeadingNumeral = canAndShouldIncrementLeadingNumeral(
					candidate, indexOfNumeralToIncrement);				
					
			if (canAndShouldIncrementLeadingNumeral){
				leadingPortionOfCandidate = candidate.substring(0, indexOfNumeralToIncrement);
				letterToInsert = this.twiceLargestNumeral;								
			} else {
				leadingPortionOfCandidate = candidate.substring(0, indexBeginningOfQuad);
				letterToInsert = this.nextLargestIndividualNumeral;
			}

			return concatenateFragmentsInOrder(
					leadingPortionOfCandidate, 
					substitutedLetter,
					letterToInsert,
					candidateTrailingRegion);
		}

		private String concatenateFragmentsInOrder(String... fragments){
			String out = "";
			for (int i = 0; i < fragments.length; i++) {
				out+=fragments[i];
			}
			return out;
		}

		private String getCandidateTrailingRegion(String candidate,
				int indexBeginningOfQuad) {
			int lengthOfRepeatRegion = this.quadRepeat.length();
			return candidate.substring(indexBeginningOfQuad+lengthOfRepeatRegion, candidate.length());
		}

		private boolean canAndShouldIncrementLeadingNumeral(String candidate,
				int indexOfNumeralToIncrement) {			
			if(doesContainAnyLeadingNumeral(indexOfNumeralToIncrement)){
				String numeralToConsiderIncrementing = Character.toString(candidate.charAt(indexOfNumeralToIncrement));
				boolean shouldAdjacentNumeralBeIncremented = numeralToConsiderIncrementing.equals(nextLargestIndividualNumeral);
				return shouldAdjacentNumeralBeIncremented && this.isTwiceLargerNumeralInDictionary;
			}
			return false;
		}

		private boolean cannotIncrementNumeral() {
			return !this.isLargerNumeralInDictionary;
		}

		private boolean doesContainAnyLeadingNumeral(
				int indexOfNumeralToIncrement) {
			return indexOfNumeralToIncrement>-1;
		}

		private boolean doesNotContainQuadRepeatForNumeral(int indexOfQuad) {
			return indexOfQuad==searchFailureCode;
		}
	}
	
	List<SubstituteAndIncrementAgent> substitutionAgents = new ArrayList<SubstituteAndIncrementAgent>();
	private static NumberAndNumeralDictionary dictionary = new NumberAndNumeralDictionary();
	
	NumberToNumeralFinalizer() {
		Iterator<String> numerals = dictionary.getDescendingRomanNumeralsIterator();
		while(numerals.hasNext()){
			substitutionAgents.add(new SubstituteAndIncrementAgent(numerals.next()));
		}
	}

	@Override
	public String substituteNumeralsIfNeeded(String numeralString) {
		for (SubstituteAndIncrementAgent substitutionAgent : substitutionAgents) {
			numeralString = substitutionAgent.substituteQuadRepeats(numeralString);
		}
		return numeralString;
	}

	public List<SubstituteAndIncrementAgent> getSubstitutionAgents() {
		return substitutionAgents;
	}	
}
