package romanNumerals;

import romanNumerals.application.RomanNumeralsApplicationController;


public class Launcher {

	public static void main(String[] args) {
		RomanNumeralsApplicationControllerInterface controller = RomanNumeralsApplicationController.create();
		System.out.println("");
		System.out.println("");
		controller.convertFromCommandLineArgs(args);
		System.out.println("");		
	}	
}
