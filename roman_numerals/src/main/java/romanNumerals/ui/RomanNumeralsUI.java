package romanNumerals.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.lang.reflect.InvocationTargetException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import romanNumerals.RomanNumeralsUIInterface;
import romanNumerals.application.RomanNumeralsApplicationController.TextInputDispatchInterface;

public class RomanNumeralsUI implements RomanNumeralsUIInterface{

	
	public static class RomanNumeralsEdtLaunchWrapper implements RomanNumeralsUIInterface{

		private RomanNumeralsUIInterface ui;

		RomanNumeralsEdtLaunchWrapper(RomanNumeralsUIInterface ui) {
			this.ui = ui;
		}
		
		public RomanNumeralsUIInterface getUi() {
			return ui;
		}
		
		@Override
		public void initializeComponents() {
			try {
				SwingUtilities.invokeAndWait(new Runnable(){
					@Override
					public void run() {
						ui.initializeComponents();
					}
				});
			} catch (InvocationTargetException | InterruptedException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void show() {
			try {
				SwingUtilities.invokeAndWait(new Runnable(){
					@Override
					public void run() {
						ui.show();
					}
				});
			} catch (InvocationTargetException | InterruptedException e) {
				e.printStackTrace();
			}			
		}

		@Override
		public void listenForTranslationRequests(TextInputDispatchInterface inputDispatch) {
			ui.listenForTranslationRequests(inputDispatch);
		}

		@Override
		public void setNumeralDisplay(String numerals) {
			ui.setNumeralDisplay(numerals);
		}
	}
	
	private class TextFieldValueListener implements DocumentListener{
		
		private TextInputDispatchInterface inputDispatch;
		
		public TextFieldValueListener(TextInputDispatchInterface inputDispatch) {
			this.inputDispatch = inputDispatch;
		}
		
		@Override
		public void changedUpdate(DocumentEvent e) {
		}
		
		@Override
		public void insertUpdate(DocumentEvent e) {	
			dispatchEvent(e);
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			dispatchEvent(e);
		}
		
		private void dispatchEvent(DocumentEvent e) {
			Document document = e.getDocument();
			String contentsOfEntireField = "";
			try {
				contentsOfEntireField = document.getText(0, document.getLength());
			} catch (BadLocationException e1) {
			}
			this.inputDispatch.processInput(contentsOfEntireField);
		}
		
	}
	
	
	public static RomanNumeralsUIInterface build() {
		return new RomanNumeralsEdtLaunchWrapper(new RomanNumeralsUI());
	}
	
	public static void main(String[] args) {
		final RomanNumeralsUIInterface ui = new RomanNumeralsUI();
		Runnable showUI = new Runnable() {
			
			@Override
			public void run() {
				ui.initializeComponents();
				ui.show();
			}
		};
		
		try {
			SwingUtilities.invokeAndWait(showUI);
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	int primarySpacingPixels = 100;
	int textFieldWidthPixels = 300;
	int textFieldHeightPixels = 150;
	int generalFontSize = 24;
	int borderComponentWidth = 8;
	
	Color backgroundColor = Color.BLACK;
	Color decorationColor = Color.WHITE;

	Font applicationFont = new Font("Serif", Font.BOLD, generalFontSize);
	Font arrowFont = new Font("Monospaced", Font.BOLD, generalFontSize);
	
	LineBorder whiteInnerBorder = new LineBorder(Color.WHITE, borderComponentWidth);
	LineBorder greyOuterBorder = new LineBorder(Color.DARK_GRAY, borderComponentWidth);
	CompoundBorder textFieldBorder = new CompoundBorder(greyOuterBorder, whiteInnerBorder);

	JTextField numberEntryField;
	JTextArea numeralDisplayField;
	JFrame window;	
	
	RomanNumeralsUI() {
	}
	
	@Override
	public void initializeComponents() {

		JLabel centerDecoration = createArrorDecoration();
		numberEntryField = createNumberEntryField("Enter number here...");
		numeralDisplayField = createNumeralEntryField("MMCXLII");
		JPanel mainPanel = createMainPanel();				

		int totalWidthMainPanel = addMainPanelComponents(
				centerDecoration,
				numberEntryField,
				numeralDisplayField,
				mainPanel);		
		
		String figureTitle = "Roman Numeral Converter";
		JPanel northDummyPanel = createDummyPanel();
		JPanel southDummyPanel = createDummyPanel();
		
		window = createWindowAndAddPanels(
				mainPanel,
				northDummyPanel,
				southDummyPanel,
				totalWidthMainPanel,
				figureTitle);
	}
	
	@Override
	public void show(){
		if (window!=null){
			window.setVisible(true);			
		}
	}
	
	@Override
	public void listenForTranslationRequests(TextInputDispatchInterface inputDispatch) {
		TextFieldValueListener textFieldValueListener = new TextFieldValueListener(inputDispatch);
		this.numberEntryField.getDocument().addDocumentListener(textFieldValueListener);
	}

	@Override
	public void setNumeralDisplay(String numerals) {
		this.numeralDisplayField.setText(numerals);
	}

	private int addMainPanelComponents(JLabel centerDecoration,
			JTextField numberEntryField, JTextArea numeralDisplayField,
			JPanel textFieldPanel) {
		textFieldPanel.add(Box.createHorizontalStrut(primarySpacingPixels));
		textFieldPanel.add(numberEntryField);
		textFieldPanel.add(centerDecoration);
		textFieldPanel.add(numeralDisplayField);
		textFieldPanel.add(Box.createHorizontalStrut(primarySpacingPixels));
		int totalWidthTextFieldPanel = 3*primarySpacingPixels+2*textFieldWidthPixels;
		return totalWidthTextFieldPanel;
	}

	private JPanel createMainPanel() {
		JPanel textFieldPanel = new JPanel();		
		textFieldPanel.setBackground(backgroundColor);
		textFieldPanel.setLayout(new BoxLayout(textFieldPanel, BoxLayout.X_AXIS));
		return textFieldPanel;
	}

	private JFrame createWindowAndAddPanels(JPanel textFieldPanel,
			JPanel northDummyPanel, JPanel southDummyPanel,
			int totalWidthComponents, String figureTitle) {
		JFrame window = new JFrame(figureTitle);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLocation(new Point(200,200));
		window.getContentPane().add(textFieldPanel);
		window.getContentPane().add(BorderLayout.NORTH, northDummyPanel);
		window.getContentPane().add(BorderLayout.SOUTH, southDummyPanel);
		
		window.setMinimumSize(new Dimension(totalWidthComponents, 1));
		window.pack();
		return window;
	}

	private JPanel createDummyPanel() {
		JPanel northDummyPanel = new JPanel();
		setDummyPanelProperties(northDummyPanel);
		return northDummyPanel;
	}

	private JTextArea createNumeralEntryField(String startingTextNumeralField) {
		JTextArea field = new JTextArea(startingTextNumeralField);
		field.setLineWrap(true);
		setFontAndBorder(field);
		field.setEditable(false);
		return field;
	}

	private JTextField createNumberEntryField(String startingTextNumberEntry) {
		JTextField numberEntryField = new JTextField(startingTextNumberEntry);
		setFontAndBorder(numberEntryField);
		return numberEntryField;
	}

	private JLabel createArrorDecoration() {
		JLabel centerDecoration = new JLabel("->");
		centerDecoration.setMinimumSize(new Dimension(primarySpacingPixels, textFieldHeightPixels));
		centerDecoration.setPreferredSize(new Dimension(primarySpacingPixels, textFieldHeightPixels));
		centerDecoration.setFont(arrowFont);
		centerDecoration.setForeground(decorationColor);
		centerDecoration.setHorizontalAlignment(SwingConstants.CENTER);
		centerDecoration.setVerticalAlignment(SwingConstants.CENTER);
		return centerDecoration;
	}

	private void setDummyPanelProperties(JPanel dummyPanel) {
		dummyPanel.setPreferredSize(new Dimension(0, primarySpacingPixels));
		dummyPanel.setBackground(backgroundColor);
	}

	private void setFontAndBorder(JComponent component) {
		component.setFont(applicationFont);
		component.setBorder(textFieldBorder);
		component.setMinimumSize(new Dimension(textFieldWidthPixels, textFieldHeightPixels));
		component.setPreferredSize(new Dimension(textFieldWidthPixels, textFieldHeightPixels));
	}
}
