package romanNumerals;

import romanNumerals.application.RomanNumeralsApplicationController.TextInputDispatchInterface;

public interface RomanNumeralsUIInterface {

	public void initializeComponents();

	public void show();

	public void listenForTranslationRequests(TextInputDispatchInterface inputDispatch);

	public void setNumeralDisplay(String numerals);
}
