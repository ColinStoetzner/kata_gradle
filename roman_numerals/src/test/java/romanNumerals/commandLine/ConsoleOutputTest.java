package romanNumerals.commandLine;

import org.junit.Test;

import romanNumerals.TestTools;
import romanNumerals.application.ConsoleOutputInterface;

public class ConsoleOutputTest extends TestTools{

	@Test
	public void testImplementsInterface() throws Exception {
		checkImplementsInterface(ConsoleOutputInterface.class, new ConsoleOutput());
	}	
}
