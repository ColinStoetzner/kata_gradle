package romanNumerals.commandLine;

import static org.junit.Assert.*;

import org.junit.Test;

import romanNumerals.TestTools;
import romanNumerals.application.LaunchArgumentParserInterface;


public class LaunchArgumentParserTest extends TestTools{

	LaunchArgumentParser launchArgumentParser = new LaunchArgumentParser();

	@Test
	public void testInterface() throws Exception {
		checkImplementsInterface(LaunchArgumentParserInterface.class, launchArgumentParser);
	}
	
	@Test
	public void testParseNewArguments() throws Exception {
		String[] args = new String[0];
		launchArgumentParser.parse(args);
		assertSame(args, launchArgumentParser.getCurrentArgs());
		String[] argsDifferent = new String[0];
		launchArgumentParser.parse(argsDifferent);
		assertSame(argsDifferent, launchArgumentParser.getCurrentArgs());
	}
	
	@Test
	public void testCheckIfHasValidNumericalInput() throws Exception {
		launchArgumentParser.parse(new String[0]);
		assertFalse(launchArgumentParser.foundValidNumber());
	}
	
	@Test
	public void testInvalidNumericalInput_empty() throws Exception {
		launchArgumentParser.parse(new String[0]);
		assertFalse(launchArgumentParser.foundValidNumber());
	}

	@Test
	public void testInvalidNumericalInput_null() throws Exception {
		try {
			launchArgumentParser.parse(null);
			assertFalse(launchArgumentParser.foundValidNumber());					
		} catch (NullPointerException e) {
			fail("should not throw");
		}
	}
	
	@Test
	public void testInvalidNumericalInput_emptyString() throws Exception {
		launchArgumentParser.parse(new String[] {""});
		assertFalse(launchArgumentParser.foundValidNumber());				
	}
	
	@Test
	public void testInvalidNumericalInput_mixedStringDoesNotCount() throws Exception {
		launchArgumentParser.parse(new String[] {"ABC123"});
		assertFalse(launchArgumentParser.foundValidNumber());				
	}
	
	@Test
	public void testValidNumericalInput() throws Exception {
		launchArgumentParser.parse(new String[] {"123"});
		assertTrue(launchArgumentParser.foundValidNumber());
		assertEquals(123, launchArgumentParser.getNumberToConvert());
	}
	
	@Test
	public void testValidNumericalInput_takeFirstValidNumericArgument() throws Exception {
		launchArgumentParser.parse(new String[] {"ABC", "234"});
		assertTrue(launchArgumentParser.foundValidNumber());				
		assertEquals(234, launchArgumentParser.getNumberToConvert());
	}
	
	@Test
	public void testValidNumericalInput_ignoreSecondValidNumericArg() throws Exception {
		launchArgumentParser.parse(new String[] {"987", "654"});
		assertTrue(launchArgumentParser.foundValidNumber());				
		assertEquals(987, launchArgumentParser.getNumberToConvert());
	}
	
	@Test
	public void testInputSeries_returnCurrentValue() throws Exception {
		launchArgumentParser.parse(new String[] {"123"});
		launchArgumentParser.parse(new String[] {"246"});
		assertTrue(launchArgumentParser.foundValidNumber());
		assertEquals(246, launchArgumentParser.getNumberToConvert());
	}
	
	@Test
	public void testInputSeries_returnCurrentStatus() throws Exception {
		launchArgumentParser.parse(new String[] {"123"});
		launchArgumentParser.parse(new String[] {"NOT A NUMBER"});
		assertFalse(launchArgumentParser.foundValidNumber());
	}
}
