package romanNumerals;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import romanNumerals.conversion.NumberToNumeralConverter;

public class TestTools {

	protected static String defaultMockString = "DEFAULT, PLEASE DEFINE";
	protected static int defaultMockInt = -123456789;

	protected HashMap<String, Method> getNamedMethods() {
		Method[] declaredMethods = NumberToNumeralConverter.class.getDeclaredMethods();
		HashMap<String, Method> namedMethods = new HashMap<String,Method>();
		for (int i = 0; i < declaredMethods.length; i++) {
			Method method = declaredMethods[i];
			namedMethods.put(method.getName(), method);
		}
		return namedMethods;
	}

	protected void checkThatNamedMethodIsNotPublic(String methodName,
			HashMap<String, Method> namedMethods) {
		Method query = namedMethods.get(methodName);
		assertNotNull("Could not find method named: " + methodName, query);
		assertFalse(methodName + " method should not be public", Modifier.isPublic(query.getModifiers()));
	}

	protected void checkNoPublicConstructors(
			Class<?> clazz) {
		assertEquals(0, clazz.getConstructors().length);
	}
	
	protected void checkImplementsInterface(
			Class<?> expectedInterface, Object instance) {
		Class<?>[] interfaces = instance.getClass().getInterfaces();
		Set<Class> namedInterfaces = new HashSet<Class>();
		for (int i = 0; i < interfaces.length; i++) {
			namedInterfaces.add(interfaces[i]);
		}
		assertTrue("Should implement: " + expectedInterface.getSimpleName(), namedInterfaces.contains(expectedInterface));
	}
	
	@SuppressWarnings("unchecked")
	protected <T> T checkAndReturnAsType(Class<T> type, Object instance) {
		assertNotNull("Instance of: [" + type.getSimpleName() + "] is null", instance);
		assertEquals("Instance is not of type: " + type.getName(), type, instance.getClass());
		return (T) instance;
	}

	protected String createRandomString() {
		return "RandomString-"+Math.random();
	}
	
	protected int createRandomPositiveInteger() {
		return (int)(Math.random()*Integer.MAX_VALUE);
	}


}
