package romanNumerals.ui;

import static org.junit.Assert.*;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import romanNumerals.RomanNumeralsUIInterface;
import romanNumerals.TestTools;
import romanNumerals.application.RomanNumeralsApplicationController.TextInputDispatchInterface;
import romanNumerals.ui.RomanNumeralsUI.RomanNumeralsEdtLaunchWrapper;

public class RomanNumeralsUITest extends TestTools{

	RomanNumeralsUI ui = new RomanNumeralsUI();

	@Before
	public void setup() {
		ui.initializeComponents();
	}

	@Test
	public void testPackageOnlyConstruction() throws Exception {
		checkNoPublicConstructors(ui.getClass());
		checkNoPublicConstructors(RomanNumeralsEdtLaunchWrapper.class);
	}
	
	@Test
	public void testImplementsExternalInterface() throws Exception {
		checkImplementsInterface(RomanNumeralsUIInterface.class, ui);
	}
	
	@Test
	public void testFactoryMethodWrapsUIForThreadSafety() throws Exception {
		RomanNumeralsUIInterface uiInterface = RomanNumeralsUI.build();
		RomanNumeralsEdtLaunchWrapper wrapper = checkAndReturnAsType(RomanNumeralsEdtLaunchWrapper.class, uiInterface);
		checkAndReturnAsType(RomanNumeralsUI.class, wrapper.getUi());
	}
	
	@Test
	public void testInitializeComponentsBuildsComponents() throws Exception {
		RomanNumeralsUI uiBuild = new RomanNumeralsUI();
		assertNull(uiBuild.window);
		assertNull(uiBuild.numberEntryField);
		assertNull(uiBuild.numeralDisplayField);
		
		uiBuild.initializeComponents();
		
		checkAndReturnAsType(JFrame.class, uiBuild.window);		
		checkAndReturnAsType(JTextField.class, uiBuild.numberEntryField);
		JTextArea numeralDisplayField = checkAndReturnAsType(JTextArea.class, uiBuild.numeralDisplayField);
		
		assertFalse("Users should not be able to edit this field", numeralDisplayField.isEditable());
	}
	
	@Test
	public void testCallingShowIsSafe() throws Exception {
		RomanNumeralsUIInterface uiSafe = new RomanNumeralsUI();
		try {
			uiSafe.show();
		} catch (NullPointerException e) {
			fail("Should not throw.");
		}
	}
	
	@Test
	public void testDefaultTextAtLaunch() throws Exception {
		assertEquals("Roman Numeral Converter", ui.window.getTitle());
		assertEquals("Enter number here...", ui.numberEntryField.getText());
		assertEquals("MMCXLII", ui.numeralDisplayField.getText());
	}
	
	@Test
	public void testGetWholeWordIfListeningToTranslationRequest() throws Exception {
		TextInputDispatchInterface inputDispatch = Mockito.mock(TextInputDispatchInterface.class);
		ui.listenForTranslationRequests(inputDispatch);
		ui.numberEntryField.setText("Foo");
		Mockito.verify(inputDispatch).processInput("");
		Mockito.verify(inputDispatch).processInput("Foo");
		ui.numberEntryField.setText("Zoop");
		Mockito.verify(inputDispatch, Mockito.times(2)).processInput("");
		Mockito.verify(inputDispatch).processInput("Zoop");
		ui.numberEntryField.setText("");
		Mockito.verify(inputDispatch, Mockito.times(3)).processInput("");
	}
	
	@Test
	public void testSetDisplay() throws Exception {
		ui.setNumeralDisplay("Hello");
		assertEquals("Hello", ui.numeralDisplayField.getText());
		ui.setNumeralDisplay("Goodbye");
		assertEquals("Goodbye", ui.numeralDisplayField.getText());
	}
	
	@Test
	public void testWrapperIsPassthroughForEDTMethods() throws Exception {
		RomanNumeralsUI ui = Mockito.mock(RomanNumeralsUI.class);
		RomanNumeralsEdtLaunchWrapper wrapper = new RomanNumeralsEdtLaunchWrapper(ui);
		
		TextInputDispatchInterface inputDispatch = Mockito.mock(TextInputDispatchInterface.class);
		wrapper.listenForTranslationRequests(inputDispatch);
		Mockito.verify(ui).listenForTranslationRequests(inputDispatch);
		
		String randomText = createRandomString();
		wrapper.setNumeralDisplay(randomText);
		Mockito.verify(ui).setNumeralDisplay(randomText);
	}
}
