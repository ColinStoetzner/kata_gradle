package romanNumerals.application;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mockito;

import romanNumerals.RomanNumeralsApplicationControllerInterface;
import romanNumerals.RomanNumeralsUIInterface;
import romanNumerals.TestTools;
import romanNumerals.application.RomanNumeralsApplicationController.TextInputDispatchInterface;
import romanNumerals.application.RomanNumeralsApplicationController.TextInputDispatchedToConverter;
import romanNumerals.commandLine.ConsoleOutput;
import romanNumerals.commandLine.LaunchArgumentParser;
import romanNumerals.conversion.NumberToNumeralConverter;
import romanNumerals.ui.RomanNumeralsUI.RomanNumeralsEdtLaunchWrapper;

public class RomanNumeralsApplicationControllerTest extends TestTools{

	private LaunchArgumentParserMock parser;
	private ConsoleOutputMock consoleOut;
	private NumberToNumeralConverterMock converter;
	private RomanNumeralsApplicationController controller;
	private RomanNumeralsUIInterface ui;
	private RomanNumeralsApplicationControllerInterface controllerMockForDispatch;
	private TextInputDispatchedToConverter dispatch;
	
	
	static class LaunchArgumentParserMock implements LaunchArgumentParserInterface{

		private int numberToReturnFromParser = defaultMockInt;
		private boolean didParserFindValidNumber;
		private String[] argsPassedInToParse = new String[] {defaultMockString};

		@Override
		public int getNumberToConvert() {
			return this.numberToReturnFromParser;
		}
		
		public void setNumberToReturnFromParser(int numberToReturnFromParser) {
			this.numberToReturnFromParser = numberToReturnFromParser;
		}
		
		@Override
		public boolean foundValidNumber() {
			return this.didParserFindValidNumber;
		}
		
		public void setDidParserFindValidNumber(boolean didParserFindValidNumber) {
			this.didParserFindValidNumber = didParserFindValidNumber;
		}

		@Override
		public void parse(String[] args) {
			this.argsPassedInToParse = args;
		}
		
		public String[] getArgsPassedInToParse() {
			return argsPassedInToParse;
		}
	}
	
	static class ConsoleOutputMock implements ConsoleOutputInterface{

		private String outputToSendToConsole = defaultMockString;

		@Override
		public void sendToConsole(String output) {
			this.outputToSendToConsole = output;
		}
		
		public String getOutputToSendToConsole() {
			return outputToSendToConsole;
		}
	}
	
	static class NumberToNumeralConverterMock implements NumberToNumeralConverterInterface{

		private int numberToConvert = defaultMockInt;
		private String numeralStringToReturn = defaultMockString;

		@Override
		public String convertNumberToNumeral(int number) {
			this.numberToConvert = number;
			return this.numeralStringToReturn;
		}
		
		public void setNumeralStringToReturn(String numeralStringToReturn) {
			this.numeralStringToReturn = numeralStringToReturn;
		}
		
		public int getNumberToConvert() {
			return numberToConvert;
		}
	}
	
	@Before
	public void setup() {
		converter = new NumberToNumeralConverterMock();
		parser = new LaunchArgumentParserMock();
		consoleOut = new ConsoleOutputMock();
		
		ui = Mockito.mock(RomanNumeralsUIInterface.class);
		
		controller = new RomanNumeralsApplicationController(converter, parser, consoleOut, ui);

		controllerMockForDispatch = Mockito.mock(RomanNumeralsApplicationControllerInterface.class);
		dispatch = new TextInputDispatchedToConverter(controllerMockForDispatch);
	}
	
	@Test
	public void testImplementsInterface() throws Exception {
		checkImplementsInterface(RomanNumeralsApplicationControllerInterface.class, controller);
	}
	
	@Test
	public void testGetters() throws Exception {
		assertSame(converter, controller.getConverter());
		assertSame(parser, controller.getParser());
		assertSame(consoleOut, controller.getConsoleOut());
		assertSame(ui, controller.getUI());
	}
		
	@Test
	public void testNoPublicConstructor() throws Exception {
		checkNoPublicConstructors(controller.getClass());
	}
	
	@Test
	public void testFactory() throws Exception {
		RomanNumeralsApplicationControllerInterface controllerInterface = RomanNumeralsApplicationController.create();
		RomanNumeralsApplicationController controller = 
				checkAndReturnAsType(RomanNumeralsApplicationController.class, controllerInterface);
		checkAndReturnAsType(NumberToNumeralConverter.class, controller.getConverter());
		checkAndReturnAsType(ConsoleOutput.class, controller.getConsoleOut());
		checkAndReturnAsType(LaunchArgumentParser.class, controller.getParser());
		checkAndReturnAsType(RomanNumeralsEdtLaunchWrapper.class, controller.getUI());
	}

	@Test
	public void testConvertFromCommandLine_alwaysSendArgsToParser() throws Exception {
		String[] args = new String[0];
		controller.convertFromCommandLineArgs(args);
		assertSame(args, parser.getArgsPassedInToParse());
	}
	
	@Test
	public void testWriteConvertedOutputToConsoleIfNumericalArgIsPresent() throws Exception {
		parser.setDidParserFindValidNumber(true);
		int parsedInteger = createRandomPositiveInteger();
		parser.setNumberToReturnFromParser(parsedInteger);
		String numeralFromConverter = "FromConverter-"+createRandomString();
		converter.setNumeralStringToReturn(numeralFromConverter);
		
		controller.convertFromCommandLineArgs(null);
		
		assertEquals(parsedInteger, converter.getNumberToConvert());
		assertEquals(numeralFromConverter, consoleOut.getOutputToSendToConsole());
	}
	
	@Test
	public void testIfNoValidNumericArguments_writeNotificationToConsoleAndDontCallConverterDirectly() throws Exception {
		parser.setDidParserFindValidNumber(false);
				
		controller.convertFromCommandLineArgs(null);

		checkThatConverterIsNotCalled();

		assertEquals("No arguments contain valid numeric input, launching UI by default.", consoleOut.getOutputToSendToConsole());
	}

	@Test
	public void testIfNoValidNumericArguments_launchUIAndConnectListener() throws Exception {
		parser.setDidParserFindValidNumber(false);
		controller.convertFromCommandLineArgs(null);
		
		ArgumentCaptor<TextInputDispatchInterface> captor = ArgumentCaptor.forClass(TextInputDispatchInterface.class);
		InOrder inOrder = Mockito.inOrder(ui);
		inOrder.verify(ui).initializeComponents();
		inOrder.verify(ui).show();
		inOrder.verify(ui).listenForTranslationRequests(captor.capture());
		
		TextInputDispatchedToConverter dispatchToConverter = checkAndReturnAsType(TextInputDispatchedToConverter.class, captor.getValue());
		assertSame(controller, dispatchToConverter.getController());
	}
	
	@Test
	public void testNumeralFromUI_isProcessedByParser() throws Exception {
		String randomTextInput = createRandomString();
		controller.convertFromUI(randomTextInput);
		
		String[] argsPassedInToParse = parser.getArgsPassedInToParse();
		assertEquals(1, argsPassedInToParse.length);
		assertEquals(randomTextInput, argsPassedInToParse[0]);		
	}

	@Test
	public void testNumeralFromUI_isConvertedIfNumeric() throws Exception {
		int randomNumberInput = createRandomPositiveInteger();
		parser.setDidParserFindValidNumber(true);
		parser.setNumberToReturnFromParser(randomNumberInput);
		
		String randomNumeralReturn = createRandomString();
		converter.setNumeralStringToReturn(randomNumeralReturn);
		
		controller.convertFromUI(null);
	
		assertEquals(randomNumberInput, converter.getNumberToConvert());
		Mockito.verify(ui).setNumeralDisplay(randomNumeralReturn);
	}
	
	@Test
	public void testNumeralFromUI_isNotConvertedIfNotNumeric() throws Exception {
		int numberThatShouldNotBeDetected = createRandomPositiveInteger();
		parser.setDidParserFindValidNumber(false);
		parser.setNumberToReturnFromParser(numberThatShouldNotBeDetected);
		controller.convertFromUI(null);		
		checkThatConverterIsNotCalled();
	}

	@Test
	public void testNumeralFromUI_uiSetToEmptyIfNotNumeric() throws Exception {	
		parser.setDidParserFindValidNumber(false);
		controller.convertFromUI(null);	
		Mockito.verify(ui).setNumeralDisplay("");
	}
	
	private void checkThatConverterIsNotCalled() {
		assertEquals(defaultMockInt, converter.getNumberToConvert());
	}
	
	@Test
	public void testInputDispatch_invisibleOutsidePackage() throws Exception {
		checkNoPublicConstructors(dispatch.getClass());
	}

	@Test
	public void testInputDispatch_implementsInterface() throws Exception {
		checkImplementsInterface(TextInputDispatchInterface.class, dispatch);
	}
	
	@Test
	public void testInputDispatch_getters() throws Exception {
		assertSame(controllerMockForDispatch, dispatch.getController());		
	}
		
	@Test
	public void testInputDispatch_callsConvertFromUI() throws Exception {
		String randomText = createRandomString();
		dispatch.processInput(randomText);
		Mockito.verify(controllerMockForDispatch).convertFromUI(randomText);
	}
}
