package romanNumerals.conversion;
import static org.junit.Assert.*;

import org.junit.Test;

import romanNumerals.TestTools;
import romanNumerals.conversion.NumberToNumeralPreprocessor;


public class NumberToNumeralPreprocessorTest extends TestTools {

	NumberToNumeralPreprocessor converter = new NumberToNumeralPreprocessor();

	@Test
	public void testNoPublicConstructor() throws Exception {
		checkNoPublicConstructors(converter.getClass());
	}

	@Test
	public void testImplementsInterface() throws Exception {
		checkImplementsInterface(NumberToNumeralPreprocessorInterface.class, converter);
	}
	
	@Test
	public void testSufficientToConvertSingleNumerals() throws Exception {
		assertEquals("I", converter.convertToNumeralWithoutSubstitution(1));
		assertEquals("V", converter.convertToNumeralWithoutSubstitution(5));
		assertEquals("X", converter.convertToNumeralWithoutSubstitution(10));
		
		assertEquals("L", converter.convertToNumeralWithoutSubstitution(50));
		assertEquals("C", converter.convertToNumeralWithoutSubstitution(100));
	}
	
	@Test
	public void testSufficientToConvertSimpleNumeralRepetition() throws Exception {
		assertEquals("III", converter.convertToNumeralWithoutSubstitution(3));
		assertEquals("XX", converter.convertToNumeralWithoutSubstitution(20));
		assertEquals("CCC", converter.convertToNumeralWithoutSubstitution(300));
	}
	
	@Test
	public void testSufficientToConvertMixedNumeralCasesWithSimpleRepetition() throws Exception {
		assertEquals("VII", converter.convertToNumeralWithoutSubstitution(7));
		assertEquals("XXVIII", converter.convertToNumeralWithoutSubstitution(28));
		assertEquals("LXXXV", converter.convertToNumeralWithoutSubstitution(85));
		assertEquals("CCLXXXIII", converter.convertToNumeralWithoutSubstitution(283));
	}
	
	@Test
	public void testRepeatFourNumeralsInDescendingOrderInsteadOfSubstitution() throws Exception {
		assertEquals("IIII", converter.convertToNumeralWithoutSubstitution(4));
		assertEquals("VIIII", converter.convertToNumeralWithoutSubstitution(9));
		assertEquals("XXXXV", converter.convertToNumeralWithoutSubstitution(45));
		assertEquals("LXXXXVIIII", converter.convertToNumeralWithoutSubstitution(99));
		assertEquals("CCCC", converter.convertToNumeralWithoutSubstitution(400));
	}
}
