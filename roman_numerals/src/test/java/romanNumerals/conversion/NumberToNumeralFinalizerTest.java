package romanNumerals.conversion;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import romanNumerals.TestTools;
import romanNumerals.conversion.NumberToNumeralFinalizer.SubstituteAndIncrementAgent;

public class NumberToNumeralFinalizerTest extends TestTools{

	NumberToNumeralFinalizer finalizer = new NumberToNumeralFinalizer();
	NumberAndNumeralDictionary dictionary = new NumberAndNumeralDictionary();

	@Test
	public void testFinalizer_noPublicConstructor() throws Exception {
		checkNoPublicConstructors(NumberToNumeralFinalizer.class);
	}
	
	@Test
	public void testFinalizer_assertsInterface() throws Exception {
		checkImplementsInterface(NumberToNumeralFinalizerInterface.class, new NumberToNumeralFinalizer());
	}

	@Test
	public void testFinalizer_getSubstitutionAgentForEachNumeralInDictionary() throws Exception {
		Iterator<String> descendingNumeralEntries = dictionary.getDescendingRomanNumeralsIterator();
		
		for (SubstituteAndIncrementAgent substituteAndIncrementAgent : finalizer.getSubstitutionAgents()) {
			String letterToSubstitute = substituteAndIncrementAgent.getLetterToSubstitute();
			assertTrue("Should be a next entry for: " + letterToSubstitute, descendingNumeralEntries.hasNext());
			assertEquals(descendingNumeralEntries.next(), letterToSubstitute);
		}
		
		String failMessage = "Though some won't have an effect due to preprocessor (eg: 'V'),"
				+ " simplest if no entries are omitted from substitution agent set.";
		assertFalse(failMessage, descendingNumeralEntries.hasNext());
	}

	@Test
	public void testFinalizer_canSubstituteAndInjectLargerNumeral() throws Exception {
		assertEquals("IV", finalizer.substituteNumeralsIfNeeded("IIII"));
		assertEquals("XL", finalizer.substituteNumeralsIfNeeded("XXXX"));
	}	

	@Test
	public void testFinalizer_canSubstituteAndIncrement() throws Exception {
		assertEquals("IX", finalizer.substituteNumeralsIfNeeded("VIIII"));
		assertEquals("XC", finalizer.substituteNumeralsIfNeeded("LXXXX"));
		assertEquals("CM", finalizer.substituteNumeralsIfNeeded("DCCCC"));
	}
	
	@Test
	public void testFinalizer_canSubstituteAndInsertIncrementedValues() throws Exception {
		assertEquals("XIX", finalizer.substituteNumeralsIfNeeded("XVIIII"));
		assertEquals("CXC", finalizer.substituteNumeralsIfNeeded("CLXXXX"));
		assertEquals("MCM", finalizer.substituteNumeralsIfNeeded("MDCCCC"));
	}
	
	private String repeatNumeralNTimes(String largestNumeral,
			int queryRepeatLength) {
		String query = "";
		for (int i = 0; i < queryRepeatLength ; i++) {
			query += largestNumeral;			
		}
		return query;
	}
	
	
	SubstituteAndIncrementAgent subAgent_I = new SubstituteAndIncrementAgent("I");
	SubstituteAndIncrementAgent subAgent_X = new SubstituteAndIncrementAgent("X");
	
	@Test
	public void testNoPublicConstructor() throws Exception {
		checkNoPublicConstructors(SubstituteAndIncrementAgent.class);
	}
	
	@Test
	public void testSubstitutionAgent_moveAQuadRepeatOfNumeralToPreviousCharacterAndIncrementNumeral() throws Exception {
		assertEquals("IX", subAgent_I.substituteQuadRepeats("VIIII"));
		assertEquals("XC", subAgent_X.substituteQuadRepeats("LXXXX"));
	}
	
	@Test
	public void testSubstitutionAgent_ifNotPossibleToIncrementCreateLargerNumeral() throws Exception {
		assertEquals("IV", subAgent_I.substituteQuadRepeats("IIII"));
		assertEquals("XIV", subAgent_I.substituteQuadRepeats("XIIII"));
		assertEquals("LIV", subAgent_I.substituteQuadRepeats("LIIII"));
		
		assertEquals("CXL", subAgent_X.substituteQuadRepeats("CXXXX"));
	}
	
	@Test
	public void testSubstitutionAgent_preservePortionsOfTheNumeralSeriesThatTrailQuadRepeat() throws Exception {
		assertEquals("CXLIII", subAgent_X.substituteQuadRepeats("CXXXXIII"));
		assertEquals("XCIII", subAgent_X.substituteQuadRepeats("LXXXXIII"));
	}
	
	@Test
	public void testSubstitutionAgent_returnOriginalStringIfInputIsGarbage() throws Exception {
		assertEquals("ABCDEFG", subAgent_I.substituteQuadRepeats("ABCDEFG"));
		assertEquals("", subAgent_I.substituteQuadRepeats(""));
	}
	
	@Test
	public void testSubstitutionAgent_getter() throws Exception {
		assertEquals("I", subAgent_I.getLetterToSubstitute());
		assertEquals("V", new SubstituteAndIncrementAgent("V").getLetterToSubstitute());
	}

	@Test
	public void testSubstitutionAgent_wontSubstituteDictionaryEntriesThatCannotBeIncremented() throws Exception {
		String largestNumeral = "M";
		assertFalse(dictionary.canIncrementNumeral(largestNumeral));
		SubstituteAndIncrementAgent subAgentLargest = new SubstituteAndIncrementAgent(largestNumeral);
		String query = repeatNumeralNTimes(largestNumeral, 4);
		assertEquals(query, subAgentLargest.substituteQuadRepeats(query));
	}


}
