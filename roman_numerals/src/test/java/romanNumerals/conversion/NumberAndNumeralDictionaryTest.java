package romanNumerals.conversion;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import romanNumerals.TestTools;


public class NumberAndNumeralDictionaryTest extends TestTools{

	private NumberAndNumeralDictionary dictionary = new NumberAndNumeralDictionary();
	private List<Integer> expectedDescendingNumbers = new ArrayList<Integer>(Arrays.asList(1000, 500, 100,50,10,5,1));
	private List<String> expectedDescendingRomanNumerals = new ArrayList<String>(Arrays.asList("M","D","C","L","X","V","I"));
	private int expectedCountEntries;
	
	@Before
	public void setup() throws Exception {
		expectedCountEntries = expectedDescendingNumbers.size();
		assertEquals("These lists should be modified together",
				expectedCountEntries, expectedDescendingRomanNumerals.size());		
	}
	
	@Test
	public void testNoPublicConstructor() throws Exception {
		checkNoPublicConstructors(dictionary.getClass());
	}
	
	@Test
	public void testGetIndividualNumerals() throws Exception {
		for (int i = 0; i < expectedCountEntries; i++) {
			Integer number = expectedDescendingNumbers.get(i);
			String failMessage = "Lookup failed with number: " + number;
			assertEquals(failMessage,
					expectedDescendingRomanNumerals.get(i),
					dictionary.getNumeral(number));			
		}
	}

	@Test
	public void testQueryFailureIsEmptyString() throws Exception {
		assertEquals("", dictionary.getNumeral(-1));
		assertEquals("", dictionary.getNumeral(0));
		assertEquals("", dictionary.getNumeral(2));
		assertEquals("", dictionary.getNumeral(999999));
	}
		
	@Test
	public void testIterateThroughNumberEntriesInDescendingOrder() throws Exception {
		Iterator<Integer> iterator = dictionary.getDescendingNumberEntriesIterator();
		checkThatIteratorContainsOnlyExpectedValues(iterator, expectedDescendingNumbers);
	}
	
	@Test
	public void testIterateThroughNumeralEntriesInDescendingOrder() throws Exception {
		Iterator<String> iterator = dictionary.getDescendingRomanNumeralsIterator();
		checkThatIteratorContainsOnlyExpectedValues(iterator, expectedDescendingRomanNumerals);
	}

	
	@Test
	public void testNextLargestIndividualNumeralNumeral() throws Exception {
		for (int i = 1; i < expectedCountEntries; i++) {
			String largerNumeral = expectedDescendingRomanNumerals.get(i-1);
			String queryNumeral = expectedDescendingRomanNumerals.get(i);
			assertTrue(dictionary.canIncrementNumeral(queryNumeral));
			assertEquals(largerNumeral, dictionary.nextLargestIndividualNumeral(queryNumeral));
		}
	}
	
	@Test
	public void testLargestNumeralReturnsSelfFromQuery() throws Exception {
		String largestNumeral = expectedDescendingRomanNumerals.get(0);
		assertFalse(dictionary.canIncrementNumeral(largestNumeral));
		assertEquals(largestNumeral, dictionary.nextLargestIndividualNumeral(largestNumeral));		
	}

	@Test
	public void testReturnInputIfNoLargestValueIsAvailable() throws Exception {
		assertEquals("", dictionary.nextLargestIndividualNumeral(""));		
		assertEquals("XYZ", dictionary.nextLargestIndividualNumeral("XYZ"));		
	}
	
	private void checkThatIteratorContainsOnlyExpectedValues(
			Iterator<?> iterator, List<?> expectedValues) {
		for (Object expectedNumber : expectedValues) {
			assertTrue(failMessage(expectedNumber),iterator.hasNext());
			assertEquals(iterator.next(), expectedNumber);
		}		
		assertFalse("Iterator should be empty", iterator.hasNext());
	}

	private String failMessage(Object expectedNumber) {
		return "Iterator should have a next element for: " + expectedNumber;
	}
}
