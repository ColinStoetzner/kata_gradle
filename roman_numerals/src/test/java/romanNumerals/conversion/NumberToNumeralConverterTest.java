package romanNumerals.conversion;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import romanNumerals.TestTools;
import romanNumerals.application.NumberToNumeralConverterInterface;


public class NumberToNumeralConverterTest extends TestTools{
	
	private NumberToNumeralPreprocessorMock preprocessor; 
	private NumberToNumeralFinalizerMock finalizer;
	private NumberToNumeralConverter converter;
	
	static class NumberToNumeralPreprocessorMock implements NumberToNumeralPreprocessorInterface{
		private int numberPassedInToConverter = defaultMockInt;
		private String numeralsToReturn = defaultMockString;

		@Override
		public String convertToNumeralWithoutSubstitution(int number) {
			this.numberPassedInToConverter = number;
			return this.numeralsToReturn;
		}
		
		public void setNumeralsToReturn(String numeralsToReturn) {
			this.numeralsToReturn = numeralsToReturn;
		}
		
		public int getNumberPassedInToConverter() {
			return numberPassedInToConverter;
		}
	}
	
	static class NumberToNumeralFinalizerMock implements NumberToNumeralFinalizerInterface {
		private String preprocessedNumeralsPassedInToConverter = defaultMockString;
		private String finalizedStringToReturn = defaultMockString;

		@Override
		public String substituteNumeralsIfNeeded(String numeralString) {
			this.preprocessedNumeralsPassedInToConverter = numeralString;
			return this.finalizedStringToReturn;
		}
		
		public void setFinalizedStringToReturn(String finalizedStringToReturn) {
			this.finalizedStringToReturn = finalizedStringToReturn;
		}
		
		public String getPreprocessedNumeralsPassedInToConverter() {
			return preprocessedNumeralsPassedInToConverter;
		}
	}
	
	@Before
	public void setup() {		
		preprocessor = new NumberToNumeralPreprocessorMock();
		finalizer = new NumberToNumeralFinalizerMock();
		converter = new NumberToNumeralConverter(preprocessor, finalizer);
	}
	
	@Test
	public void testNoPublicConstructors() throws Exception {
		checkNoPublicConstructors(NumberToNumeralConverter.class);
	}
	
	@Test
	public void testAssertsInterface() throws Exception {
		checkImplementsInterface(NumberToNumeralConverterInterface.class, converter);
	}
	
	@Test
	public void testConstructWithCollaborators() throws Exception {
		assertSame(preprocessor, converter.getPreprocessor());
		assertSame(finalizer, converter.getFinalizer());
	}

	@Test
	public void testConvertAlwaysCallsPreprocessorThenFinalizer() throws Exception {
		String numeralsPreprocessorReturns = "preprocessorReturn-"+createRandomString();
		String numeralsFinalizerReturns = "finalizerReturn-"+createRandomString();
		preprocessor.setNumeralsToReturn(numeralsPreprocessorReturns);
		finalizer.setFinalizedStringToReturn(numeralsFinalizerReturns);
		
		int inputToConverter = createRandomPositiveInteger();
		String numeralReturnedFromConverter = converter.convertNumberToNumeral(inputToConverter);

		assertEquals(inputToConverter, this.preprocessor.getNumberPassedInToConverter());
		assertEquals(numeralsPreprocessorReturns, this.finalizer.getPreprocessedNumeralsPassedInToConverter());
		assertEquals(numeralsFinalizerReturns, numeralReturnedFromConverter);
	}
	
	@Test
	public void testFactoryMethod() throws Exception {
		NumberToNumeralConverterInterface converterInterface = NumberToNumeralConverter.create();
		NumberToNumeralConverter converterInstance = checkAndReturnAsType(NumberToNumeralConverter.class,
				converterInterface);
		checkAndReturnAsType(NumberToNumeralPreprocessor.class, converterInstance.getPreprocessor());
		checkAndReturnAsType(NumberToNumeralFinalizer.class, converterInstance.getFinalizer());
	}
	
	NumberToNumeralConverterInterface completeConverter = NumberToNumeralConverter.create();

	@Test
	public void testIntegrationOfConverterComponents() throws Exception {
		assertEquals("VIII", completeConverter.convertNumberToNumeral(8));
		assertEquals("XCI", completeConverter.convertNumberToNumeral(91));		
		assertEquals("XCIII", completeConverter.convertNumberToNumeral(93));		
		assertEquals("CCCXLVII", completeConverter.convertNumberToNumeral(347));
	}
	
	@Test
	public void testIntegrationOfConverterComponents_explicitKataTestCases() throws Exception {
		assertEquals("I", completeConverter.convertNumberToNumeral(1));
		assertEquals("III", completeConverter.convertNumberToNumeral(3));
		assertEquals("IX", completeConverter.convertNumberToNumeral(9));	
		assertEquals("MLXVI", completeConverter.convertNumberToNumeral(1066));	
		assertEquals("MCMLXXXIX", completeConverter.convertNumberToNumeral(1989));	
	}
}
