20161222
Kata exercise for Pillar
Colin R. Stoetzner


1) Literal instructions:

-The literal instructions here apply to a windows command line environment.

-This readme contains command line instructions. If you see ">>TEXT" (minus the quotes) that means enter the literal text "TEXT" (minus the quotes) at the command prompt and press enter. So,

>>gradlew

...would mean to exactly type the word "gradlew" (minus quotes) at the commmand prompt and press enter.


2) Build instructions:

-This exercise uses Gradle to build, test and run a Java application and associated tests.

-At minimum a Java 7 runtime should be installed to support both the application and the Gradle command line environment. On Windows, the %JAVA_HOME% environment variable should be set to the JRE or JDK installation directory.

-Eclipse project files are present in the repository but can be ignored.

-The Gradle wrapper is included in the repository and should be used to interact with the project through the command line.

-After cloning this repository, navigate to the root directory and open a command prompt. 

>>gradlew tasks

-Note a section of tasks specific for this exercise:

Kata Demo tasks
---------------
runApp - Launch with a list of command line args (with all quotes) as: gradlew runApp -PmainArgs="['abc','123']"
runTests - View individual test results in console.
viewOnlyUI - Review UI layout separately from application

-Three relevant commands are available, try the first command:

>>gradlew runApp

This will build any out of date portions of the application and show the main application. After commit [024e82b] this command is linked to the UI. Next,

>>gradlew runTests

...will run all individual unit tests in the project and give feedback about individual results in the console (also building out of date portions of the application). Note that the following built-in task,

>>gradlew test

...may not show individual test results in the console and is not recommended.

Following commit [7957c22], runApp supports passage of command line arguments to the Java main method. Note that individual arguments must be surrounded by single quotes, and packaged into groovy list syntax within double quotes. The command below passes two separate arguments: 

>>gradlew runApp -PmainArgs="['abc','123']"

-See build.gradle for build script implementation details, if desired.

Following commit [024e82b], added ability to launch stand alone UI to test resize behavior, layout and look & feel.

>>gradlew viewOnlyUI